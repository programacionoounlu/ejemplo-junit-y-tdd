package ar.edu.unlu.poo.pruebas;

public class Lista {
	private int[] elementos; 
	private int contador;
	private int max = 100;
	
	public Lista(int[] elementos) {
		this.elementos = elementos;
		max = elementos.length;
	}
	
	public Lista() {
		this.elementos = new int[max];
	}
	
	public void add(int e) {
		if (contador == max) {
			return;
		}
		elementos[contador++] = e;
	}
	
	public int getElement (int index) {
		if (index < 0 || index >= contador) {
			return -1;
		}
		return elementos[index];
	}
	
	public void remove(int index) {
		if (index < 0 || index >= contador) {
			return;
		}
		elementos[index] = 0;
	}
	
	public int size() {
		return contador;
	}
	
	public static void main (String args[]) {
		Lista l = new Lista();
		l.add(3);
		System.out.println(l.getElement(0));
		System.out.println(l.size());
	}
}
