package ar.edu.unlu.poo.pruebas;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ListaTest {
	
	private Lista lista;
	private int[] elementos;

	@Before
	public void setUp() throws Exception {
		elementos = new int[2];
		lista = new Lista(elementos);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddAgregueUnElemento() {
		// pruebo condiciones iniciales
		assertEquals(0, lista.size());
		
		// agrego un elemento
		lista.add(10);
		
		// reviso que el elemento halla sido agregado
		assertEquals(10, elementos[0]);
		assertEquals(1, lista.size());
	}
	
	@Test
	public void testAddValida() {
		// pruebo condiciones iniciales
		assertEquals(0, lista.size());
		
		// agrego elementos
		lista.add(10);
		lista.add(11);
		lista.add(12);
		
		// reviso que el elemento halla sido agregado
		assertEquals(10, elementos[0]);
		assertEquals(11, elementos[1]);
		assertEquals(2, lista.size());
	}
	
	@Test
	public void testGetElement() {
		// pruebo condiciones iniciales
		assertEquals(0, lista.size());
		
		// agrego elementos
		lista.add(10);
		
		// reviso que el elemento halla sido agregado
		assertEquals(10, elementos[0]);
		assertEquals(1, lista.size());
		
		// prueba getElement
		assertEquals(10, lista.getElement(0));
	}
	
	@Test
	public void testGetElementValida() {
		// pruebo condiciones iniciales
		assertEquals(0, lista.size());
		
		// agrego elementos
		lista.add(10);
		
		// reviso que el elemento halla sido agregado
		assertEquals(10, elementos[0]);
		assertEquals(1, lista.size());
		
		
		// probar getElement validando
		assertEquals(-1, lista.getElement(-1));
		assertEquals(-1, lista.getElement(2));
	}
}
