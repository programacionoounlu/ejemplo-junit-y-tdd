package ar.edu.unlu.poo.tdd;

public class Pila {

	private int[] elementos;
	private int index;

	public Pila(int[] elementos1) {
		elementos = elementos1;
	}

	public void apilar(int i) {
		if (index == elementos.length) {
			return;
		}
		elementos[index++] = i;
	}

	public int desapilar() {
		if (index != 0) {
			return elementos[--index];
		} else {
			return -1;
		}
	}

}
