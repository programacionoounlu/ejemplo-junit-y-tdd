package ar.edu.unlu.poo.tdd;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PilaTest {
	
	private int[] elementos;
	private Pila pila;

	@Before
	public void setUp() throws Exception {
		elementos = new int[2];
		pila = new Pila(elementos);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testApilar() {
		pila.apilar(10);
		assertEquals(10, elementos[0]);
	}
	
	@Test
	public void testApilarValida() {
		pila.apilar(10);
		pila.apilar(11);
		pila.apilar(12);
		assertEquals(10, elementos[0]);
		assertEquals(11, elementos[1]);
	}
	
	@Test
	public void testDesapilar() {
		pila.apilar(10);
		pila.apilar(11);
		assertEquals(10, elementos[0]);
		assertEquals(11, elementos[1]);
		
		assertEquals(11, pila.desapilar());
		assertEquals(10, pila.desapilar());
	}
	
	@Test
	public void testDesapilarValida() {
		assertEquals(-1, pila.desapilar());
	}

}
